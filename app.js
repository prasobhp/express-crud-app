const express = require('express');
const { ObjectId } = require('mongodb');
require('dotenv').config();
const { connectToDb, getDb }  = require('./dbconnection');
const multer = require('multer');

const app = express();

app.use(express.json());
app.use(express.static('images'));

let db;

connectToDb((err) => {
    if(!err){
        app.listen(process.env.PORT, (req, res) => {
            console.log(`Listening on ${process.env.PORT}`);
        });

        db =  getDb();
    }
});

const storage = multer.diskStorage({
    destination: function (req, file, callback) {
      callback(null, './images');
    },
    filename: function (req, file, callback) {
      callback(null, Date.now() + file.originalname);
    },
  });
  const upload = multer({
    storage: storage
  });




app.get('/students', (req, res) => {
    let students = [];
    
    let page = req.query.page || 0 ;

    let PerPage = 2 ;

    db.collection('student')
    .find()
    .sort({ name:1 })
    .skip(page * PerPage)
    .limit(PerPage)
    .forEach(student => students.push(student))
    .then(() => {
        res.status(200).json(students)
    })
    .catch(() => {
        res.status(500).json({
            error: 'Unable to fatch data'
        })
    })
});


app.delete('/students/:id', (req, res) => {
    if(ObjectId.isValid(req.params.id)){
        db.collection('student')
        .deleteOne({_id: ObjectId(req.params.id)})
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            res.status(500).json({error: "Unable to delet data"})
        })
    }
    else{
        res.status(500).json({error: "Not a valid id"})
    }
});

app.post('/students', upload.single('image'), (req, res) => {
    const img = req.file.filename;
    let image = {
        "image":img
    }
    console.log(image);
    const studentform = req.body;
    let student = Object.assign(studentform, image);

    db.collection('student')
    .insertOne(student)
    .then(result => {
        res.status(201).json(result);
    })
    .catch(error => {
        res.status(500).json({error: "Unable to create a new data"})
    })
});

app.patch('/students/:id', (req, res) => {
    const updates = req.body;

    if(ObjectId.isValid(req.params.id)){
        db.collection('student')
        .updateOne({_id: ObjectId(req.params.id)},  {$set: updates})
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            res.status(500).json({error: "Unable to update data"})
        })
    }
    else{
        res.status(500).json({error: "Not a valid id"})
    }
});